const getWindows = callback => {
  return chrome.windows.getAll({ populate: true }, callback);
};

const tabsWith = (tabs, search) => {
  return tabs.filter(tab => {
    return tab.title.toLowerCase().includes(search.toLowerCase());
  });
};

const windowsWith = (windows, search) => {
  return windows.filter(win => {
    return tabsWith(win.tabs, search).length > 0;
  });
};

const getActiveTab = tabs => {
  const active = tabs.find(t => t.active);
  if (active.title.length > 64) {
    return {
      title: active.title.substr(0, 64) + '...',
      favIconUrl: active.favIconUrl,
      id: active.id
    };
  } else {
    return {
      title: active.title,
      favIconUrl: active.favIconUrl,
      id: active.id
    };
  }
};

module.exports = {
  getWindows,
  tabsWith,
  windowsWith,
  getActiveTab
};
