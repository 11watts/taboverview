#Tab Overview

A tab management extension. Tab Overview allows you to search you open tabs and windows. You can switch to a tab or window by clicking on the title or close them by clicking the delete icon.

###[Download for firefox](https://addons.mozilla.org/en-US/firefox/addon/tab-overview/?src=search)

Build with [vue-chrome-extension-template.](https://github.com/YuraDev/vue-chrome-extension-template)

##Installation
Use `npm install` to install dependencies.

Use `npm run dev` to build to dist folder.

Use `npm run build` to build to build.zip file




